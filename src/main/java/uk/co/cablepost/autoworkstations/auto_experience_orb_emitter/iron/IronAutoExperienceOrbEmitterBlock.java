package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterBlock;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class IronAutoExperienceOrbEmitterBlock extends AutoExperienceOrbEmitterBlock {
    public IronAutoExperienceOrbEmitterBlock(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronAutoExperienceOrbEmitterBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                IronAutoExperienceOrbEmitterBlock.checkType(type, AutoWorkstations.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, IronAutoExperienceOrbEmitterBlockEntity::clientTick) :
                IronAutoExperienceOrbEmitterBlock.checkType(type, AutoWorkstations.IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, IronAutoExperienceOrbEmitterBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack itemStack, BlockView world, List<Text> tooltip, TooltipContext tooltipContext) {
        tooltip.add( translatableText("block.autoworkstations.iron_auto_experience_orb_emitter.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.redstone_to_enable.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_RED) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
