package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterBlockEntity;

public class GoldAutoExperienceOrbEmitterBlockEntity extends AutoExperienceOrbEmitterBlockEntity {
    public GoldAutoExperienceOrbEmitterBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoWorkstations.GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY, blockPos, blockState);
        xpEmptyBottleMaxProgress = 10;
        xpEmitTimerMax = 2;
    }
}
