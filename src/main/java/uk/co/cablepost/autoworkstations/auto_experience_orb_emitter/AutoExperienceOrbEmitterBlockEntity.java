package uk.co.cablepost.autoworkstations.auto_experience_orb_emitter;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoExperienceOrbEmitterBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {
    public static int INVENTORY_SIZE = 2;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static int SLOT_EXP_BOTTLE = 0;
    public static int SLOT_EMPTY_BOTTLE = 1;


    int xpEmptyBottleProgress;
    public int xpEmptyBottleMaxProgress;
    public int xpEmitTimer;
    public int xpEmitTimerMax;
    int expTank;

    public AutoExperienceOrbEmitterBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        xpEmptyBottleMaxProgress = 20;
        xpEmitTimerMax = 10;
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory);
        xpEmptyBottleProgress = nbt.getInt("XpEmptyBottleProgress");
        xpEmptyBottleMaxProgress = nbt.getInt("XpEmptyBottleMaxProgress");
        xpEmitTimer = nbt.getInt("xpEmitTimer");
        xpEmitTimerMax = nbt.getInt("xpEmitTimerMax");
        if(xpEmptyBottleMaxProgress == 0){
            xpEmptyBottleMaxProgress = 20;
        }
        expTank = nbt.getInt("ExpTank");
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        nbt.putInt("XpEmptyBottleProgress", xpEmptyBottleProgress);
        nbt.putInt("XpEmptyBottleMaxProgress", xpEmptyBottleMaxProgress);
        nbt.putInt("xpEmitTimer", xpEmitTimer);
        nbt.putInt("xpEmitTimerMax", xpEmitTimerMax);
        nbt.putInt("ExpTank", expTank);
        Inventories.writeNbt(nbt, inventory);

        super.writeNbt(nbt);
    }

    public static int PROPERTY_DELEGATE_SIZE = 3;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0: {
                    return xpEmptyBottleProgress;
                }
                case 1: {
                    return xpEmptyBottleMaxProgress;
                }
                case 2: {
                    return expTank;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoExperienceOrbEmitterBlockEntity blockEntity) {
        blockEntity.commonTick();
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoExperienceOrbEmitterBlockEntity blockEntity) {
        blockEntity.commonTick();

        boolean toMarkDirty = false;

        if(world == null){
            return;
        }

        //empty xp bottles

        ItemStack emptyBottlesStack = blockEntity.inventory.get(SLOT_EMPTY_BOTTLE);
        ItemStack expBottlesStack = blockEntity.inventory.get(SLOT_EXP_BOTTLE);

        if(
                (
                        //A glass bottle to use
                        !expBottlesStack.isEmpty() &&
                        expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE)
                )
                &&
                (
                        //space in output
                        emptyBottlesStack.isEmpty() ||
                        (
                                emptyBottlesStack.isOf(Items.GLASS_BOTTLE) &&
                                emptyBottlesStack.getCount() < emptyBottlesStack.getMaxCount()
                        )
                )
        ){
            blockEntity.xpEmptyBottleProgress += 1;

            if(blockEntity.xpEmptyBottleProgress >= blockEntity.xpEmptyBottleMaxProgress) {
                blockEntity.expTank += AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;
                expBottlesStack.decrement(1);
                if (emptyBottlesStack.isEmpty()) {
                    emptyBottlesStack = new ItemStack(Items.GLASS_BOTTLE);
                } else {
                    emptyBottlesStack.increment(1);
                }

                blockEntity.inventory.set(SLOT_EMPTY_BOTTLE, emptyBottlesStack);
                blockEntity.xpEmptyBottleProgress = 0;

                //blockEntity.markDirty(world, pos, state);
                toMarkDirty = true;
            }
        }
        else{
            blockEntity.xpEmptyBottleProgress = 0;
        }

        if(state.get(AutoExperienceOrbEmitterBlock.ENABLED) && blockEntity.expTank >= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE){
            blockEntity.xpEmitTimer += 1;
            if(blockEntity.xpEmitTimer >= blockEntity.xpEmitTimerMax){
                // emit XP
                int i = 3 + world.random.nextInt(5) + world.random.nextInt(5);
                ExperienceOrbEntity.spawn(
                        (ServerWorld)world,
                        new Vec3d(pos.getX() + 0.5f, pos.getY() + 0.25f, pos.getZ() + 0.5f),
                        i
                );

                blockEntity.expTank -= AutoWorkstations.EXPERIENCE_BOTTLE_VALUE;

                blockEntity.xpEmitTimer = 0;
            }
        }
        else {
            blockEntity.xpEmitTimer = 0;
        }

        if(toMarkDirty){
            blockEntity.markDirty();
        }
    }

    public void commonTick(){
    }

    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoExperienceOrbEmitterScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return slot == SLOT_EXP_BOTTLE && stack.isOf(Items.EXPERIENCE_BOTTLE);
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == SLOT_EMPTY_BOTTLE;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < inventory.size(); i++){
            if(!inventory.get(i).isEmpty()){
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public void dropXP(){
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), expTank);
    }
}
