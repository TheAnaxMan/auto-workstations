package uk.co.cablepost.autoworkstations.auto_anvil.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntity;

public class IronAutoAnvilBlockEntity extends AutoAnvilBlockEntity {
    public IronAutoAnvilBlockEntity(BlockPos pos, BlockState state) {
        super(AutoWorkstations.IRON_AUTO_ANVIL_BLOCK_ENTITY, pos, state);
        xpEmptyBottleMaxProgress = 20;
        maxProcessProgress = 60;
    }
}
