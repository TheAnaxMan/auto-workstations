package uk.co.cablepost.autoworkstations.auto_anvil;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.util.StringPropertyDel;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static uk.co.cablepost.autoworkstations.util.TextUtil.literalText;
import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoAnvilBlockEntity extends BlockEntity implements SidedInventory, NamedScreenHandlerFactory {

    public static int INVENTORY_SIZE = 6;
    protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public static int SLOT_EMPTY_BOTTLE = 0;
    public static int SLOT_EXP_BOTTLE = 1;
    public static int SLOT_ITEM_IN = 2;
    public static int SLOT_ITEM_ADD = 3;
    public static int SLOT_ITEM_OUT = 4;
    public static int SLOT_ANVIL = 5;

    int xpEmptyBottleProgress;
    public int xpEmptyBottleMaxProgress;
    int expLevel;
    float expProgress;

    public int processProgress;
    public int maxProcessProgress;


    public static int MODE_REPAIR = 0;
    public static int MODE_RENAME = 1;

    public int mode = MODE_REPAIR;

    public ItemStack result = null;
    public Integer levelCost = null;
    public Integer repairItemUsage = null;

    public ItemStack lastInputStack = null;
    public ItemStack lastAdditionStack = null;

    public int lastMode = -1;

    public Random random;

    public AutoAnvilBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        xpEmptyBottleMaxProgress = 20;
        maxProcessProgress = 60;

        random = new Random();
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);

        inventory = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
        Inventories.readNbt(nbt, inventory);

        xpEmptyBottleProgress = nbt.getInt("XpEmptyBottleProgress");
        xpEmptyBottleMaxProgress = nbt.getInt("XpEmptyBottleMaxProgress");
        if(xpEmptyBottleMaxProgress == 0){
            xpEmptyBottleMaxProgress = 20;
        }

        expLevel = nbt.getInt("ExpLevel");
        expProgress = nbt.getFloat("ExpProgress");

        processProgress = nbt.getInt("ProcessProgress");
        maxProcessProgress = nbt.getInt("MaxProcessProgress");
        if(maxProcessProgress == 0){
            maxProcessProgress = 60;
        }

        mode = nbt.getInt("Mode");
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);

        Inventories.writeNbt(nbt, inventory);

        nbt.putInt("XpEmptyBottleProgress", xpEmptyBottleProgress);
        nbt.putInt("XpEmptyBottleMaxProgress", xpEmptyBottleMaxProgress);
        nbt.putInt("ExpLevel", expLevel);
        nbt.putFloat("ExpProgress", expProgress);

        nbt.putInt("ProcessProgress", processProgress);
        nbt.putInt("MaxProcessProgress", maxProcessProgress);

        nbt.putInt("Mode", mode);
    }

    public static int PROPERTY_DELEGATE_SIZE = 8;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            switch (index) {
                case 0 -> {
                    return Math.round(AutoAnvilBlockEntity.this.expProgress * 100f);
                }
                case 1 -> {
                    return AutoAnvilBlockEntity.this.expLevel;
                }
                case 2 -> {
                    return xpEmptyBottleProgress;
                }
                case 3 -> {
                    return xpEmptyBottleMaxProgress;
                }
                case 4 ->{
                    return AutoAnvilBlockEntity.this.processProgress;
                }
                case 5 -> {
                    return AutoAnvilBlockEntity.this.maxProcessProgress;
                }
                case 6 -> {
                    return AutoAnvilBlockEntity.this.mode;
                }
                case 7 -> {
                    return AutoAnvilBlockEntity.this.levelCost;
                }
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            if (index == 6) {
                AutoAnvilBlockEntity.this.mode = value;
            }
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoAnvilBlockEntity blockEntity) {
        if(blockEntity.processProgress > 0){
            blockEntity.processProgress++;
        }
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoAnvilBlockEntity blockEntity) {
        boolean toMarkDirty = false;

        //empty xp bottles

        ItemStack expBottlesStack = blockEntity.inventory.get(SLOT_EXP_BOTTLE);
        ItemStack emptyBottlesStack = blockEntity.inventory.get(SLOT_EMPTY_BOTTLE);

        if(
                (
                        //An exp bottle to use
                        !expBottlesStack.isEmpty() &&
                        expBottlesStack.isOf(Items.EXPERIENCE_BOTTLE)
                )
                &&
                (
                        //space in output
                        emptyBottlesStack.isEmpty() ||
                        (
                                emptyBottlesStack.isOf(Items.GLASS_BOTTLE) &&
                                emptyBottlesStack.getCount() < emptyBottlesStack.getMaxCount()
                        )
                )
        ){
            blockEntity.xpEmptyBottleProgress += 1;

            if(blockEntity.xpEmptyBottleProgress >= blockEntity.xpEmptyBottleMaxProgress) {
                blockEntity.addExperience(Math.round(AutoWorkstations.EXPERIENCE_BOTTLE_VALUE));
                expBottlesStack.decrement(1);
                if (emptyBottlesStack.isEmpty()) {
                    emptyBottlesStack = new ItemStack(Items.GLASS_BOTTLE);
                } else {
                    emptyBottlesStack.increment(1);
                }

                blockEntity.inventory.set(SLOT_EMPTY_BOTTLE, emptyBottlesStack);
                blockEntity.xpEmptyBottleProgress = 0;

                toMarkDirty = true;
            }
        }
        else{
            blockEntity.xpEmptyBottleProgress = 0;
        }

        //--- anvil stuff ---

        ItemStack input = blockEntity.getStack(SLOT_ITEM_IN);
        ItemStack addition = blockEntity.getStack(SLOT_ITEM_ADD);

        if(
                blockEntity.lastMode != blockEntity.mode ||
                blockEntity.lastInputStack == null ||
                !blockEntity.lastInputStack.isItemEqual(input) ||
                blockEntity.lastInputStack.getCount() != input.getCount() ||
                blockEntity.lastAdditionStack == null ||
                !blockEntity.lastAdditionStack.isItemEqual(addition) ||
                blockEntity.lastAdditionStack.getCount() != addition.getCount() ||
                blockEntity.result == null
        ){
            blockEntity.lastInputStack = input.copy();
            blockEntity.lastAdditionStack = addition.copy();

            blockEntity.lastMode = blockEntity.mode;

            ItemStack resultBefore = blockEntity.result == null ? null : blockEntity.result.copy();
            blockEntity.updateResult();
            if(
                    resultBefore == null ||
                    !resultBefore.isItemEqual(blockEntity.result) ||
                    resultBefore.getDamage() != blockEntity.result.getDamage() ||
                    resultBefore.getRepairCost() != blockEntity.result.getRepairCost()
            ) {
                blockEntity.processProgress = 0;
            }
        }

        ItemStack anvilStack = blockEntity.getStack(SLOT_ANVIL);

        if(
                !blockEntity.result.isEmpty() &&
                blockEntity.levelCost <= blockEntity.expLevel &&
                blockEntity.getStack(SLOT_ITEM_OUT).isEmpty() &&
                (
                    anvilStack.isOf(Items.ANVIL) ||
                    anvilStack.isOf(Items.CHIPPED_ANVIL) ||
                    anvilStack.isOf(Items.DAMAGED_ANVIL)
                )
        ){
            blockEntity.processProgress ++;
            if(blockEntity.processProgress == 1) {//Client will do the rest
                toMarkDirty = true;
            }

            if (blockEntity.processProgress == blockEntity.maxProcessProgress - 15) {
                world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
            }

            if(blockEntity.processProgress >= blockEntity.maxProcessProgress) {
                blockEntity.doProcess();
                blockEntity.processProgress = 0;
                toMarkDirty = true;
            }
        }
        else{
            blockEntity.processProgress = 0;
        }

        //--- ---

        if (toMarkDirty) {
            blockEntity.markDirty();
        }
    }

    public void doProcess(){
        addExperienceLevels(-levelCost);

        if(random.nextFloat() < (0.12F / getStack(SLOT_ANVIL).getCount())){//Chance of damage is proportional to anvil count, 2 anvils halves the chance etc
            damageAnvilItem();
        }

        setStack(SLOT_ITEM_IN, ItemStack.EMPTY);

        if(mode == MODE_REPAIR) {
            if (repairItemUsage > 0) {
                ItemStack additionStack = getStack(SLOT_ITEM_ADD);
                if (!additionStack.isEmpty() && additionStack.getCount() > this.repairItemUsage) {
                    additionStack.decrement(this.repairItemUsage);
                    setStack(SLOT_ITEM_ADD, additionStack);
                } else {
                    setStack(SLOT_ITEM_ADD, ItemStack.EMPTY);
                }
            } else {
                setStack(SLOT_ITEM_ADD, ItemStack.EMPTY);
            }
        }

        setStack(SLOT_ITEM_OUT, result.copy());
    }

    public void damageAnvilItem(){
        ItemStack anvilStack = getStack(SLOT_ANVIL);

        if(anvilStack.isOf(Items.DAMAGED_ANVIL)){
            setStack(SLOT_ANVIL, ItemStack.EMPTY);
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_DESTROY, SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
            return;
        }

        if(anvilStack.isOf(Items.ANVIL)){
            setStack(SLOT_ANVIL, new ItemStack(Items.CHIPPED_ANVIL, anvilStack.getCount()));
        }
        if(anvilStack.isOf(Items.CHIPPED_ANVIL)){
            setStack(SLOT_ANVIL, new ItemStack(Items.DAMAGED_ANVIL, anvilStack.getCount()));
        }

        world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_BREAK, SoundCategory.BLOCKS, 0.3f, world.random.nextFloat() * 0.1f + 0.9f);
    }

    public void updateResult() {
        levelCost = 0;

        result = getStack(SLOT_ITEM_IN).copy();

        repairItemUsage = 0;

        int levelCost1 = 0;
        int levelCost2 = 0;
        int levelCost3 = 0;

        if (result.isEmpty()) {
            return;
        }

        ItemStack additionStack = this.getStack(SLOT_ITEM_ADD).copy();

        Map<Enchantment, Integer> map = EnchantmentHelper.get(result);
        levelCost2 += result.getRepairCost() + (additionStack.isEmpty() ? 0 : additionStack.getRepairCost());

        if(mode == MODE_REPAIR){
            if(additionStack.isEmpty()){
                result = ItemStack.EMPTY;
                return;
            }

            boolean bl = additionStack.isOf(Items.ENCHANTED_BOOK) && !EnchantedBookItem.getEnchantmentNbt(additionStack).isEmpty();
            int damage1;
            int damage2;
            int damage3;
            if (result.isDamageable() && result.getItem().canRepair(result, additionStack)) {
                damage1 = Math.min(result.getDamage(), result.getMaxDamage() / 4);
                if (damage1 <= 0) {
                    result = ItemStack.EMPTY;
                    return;
                }

                for(damage2 = 0; damage1 > 0 && damage2 < additionStack.getCount(); ++damage2) {
                    damage3 = result.getDamage() - damage1;
                    result.setDamage(damage3);
                    ++levelCost1;
                    damage1 = Math.min(result.getDamage(), result.getMaxDamage() / 4);
                }

                repairItemUsage = damage2;
            } else {
                if (!bl && (!result.isOf(additionStack.getItem()) || !result.isDamageable())) {
                    result = ItemStack.EMPTY;
                    return;
                }

                if (result.isDamageable() && !bl) {
                    damage1 = result.getMaxDamage() - result.getDamage();
                    damage2 = additionStack.getMaxDamage() - additionStack.getDamage();
                    damage3 = damage2 + result.getMaxDamage() * 12 / 100;
                    int damage4 = damage1 + damage3;
                    int damage5 = result.getMaxDamage() - damage4;
                    if (damage5 < 0) {
                        damage5 = 0;
                    }

                    if (damage5 < result.getDamage()) {
                        result.setDamage(damage5);
                        levelCost1 += 2;
                    }
                }

                Map<Enchantment, Integer> map2 = EnchantmentHelper.get(additionStack);
                boolean bl2 = false;
                boolean bl3 = false;
                Iterator<Enchantment> var23 = map2.keySet().iterator();

                label155:
                while(true) {
                    Enchantment enchantment;
                    do {
                        if (!var23.hasNext()) {
                            if (bl3 && !bl2) {
                                result = ItemStack.EMPTY;
                                levelCost = 0;
                                return;
                            }
                            break label155;
                        }

                        enchantment = var23.next();
                    } while(enchantment == null);

                    int q = map.getOrDefault(enchantment, 0);
                    int r = map2.get(enchantment);
                    r = q == r ? r + 1 : Math.max(r, q);
                    boolean bl4 = enchantment.isAcceptableItem(result);
                    if (result.isOf(Items.ENCHANTED_BOOK)) {
                        bl4 = true;
                    }

                    for (Enchantment enchantment2 : map.keySet()) {
                        if (enchantment2 != enchantment && !enchantment.canCombine(enchantment2)) {
                            bl4 = false;
                            ++levelCost1;
                        }
                    }

                    if (!bl4) {
                        bl3 = true;
                    } else {
                        bl2 = true;
                        if (r > enchantment.getMaxLevel()) {
                            r = enchantment.getMaxLevel();
                        }

                        map.put(enchantment, r);
                        int rarity = switch (enchantment.getRarity()) {
                            case COMMON -> 1;
                            case UNCOMMON -> 2;
                            case RARE -> 4;
                            case VERY_RARE -> 8;
                        };

                        if (bl) {
                            rarity = Math.max(1, rarity / 2);
                        }

                        levelCost1 += rarity * r;
                        if (result.getCount() > 1) {
                            levelCost1 = 40;
                        }
                    }
                }
            }
        }
        else if(mode == MODE_RENAME){
            String newName = "";
            if(!additionStack.isEmpty()){
                newName = additionStack.getName().getString();
            }

            if (StringUtils.isBlank(newName)) {
                if (result.hasCustomName()) {
                    levelCost3 = 1;
                    levelCost1 += levelCost3;
                    result.removeCustomName();
                }
            } else if (!newName.equals(result.getName().getString())) {
                levelCost3 = 1;
                levelCost1 += levelCost3;
                result.setCustomName(literalText(newName));
            }
        }

        levelCost = levelCost2 + levelCost1;

        if (levelCost1 <= 0) {
            result = ItemStack.EMPTY;
        }

        if (levelCost3 == levelCost1 && levelCost3 > 0 && levelCost >= 40) {
            levelCost = 39;
        }

        if (levelCost >= 40) {
            result = ItemStack.EMPTY;
        }

        if (!result.isEmpty()) {
            int t = result.getRepairCost();
            if (!additionStack.isEmpty() && t < additionStack.getRepairCost()) {
                t = additionStack.getRepairCost();
            }

            if (levelCost3 != levelCost1 || levelCost3 == 0) {
                t = AnvilScreenHandler.getNextCost(t);
            }

            result.setRepairCost(t);
            EnchantmentHelper.set(map, result);
        }
    }

    public void addExperience(int experience) {
        this.expProgress += (float)experience / (float)this.getNextLevelExperience();
        while (this.expProgress < 0.0f) {
            float f = this.expProgress * (float)this.getNextLevelExperience();
            if (this.expLevel > 0) {
                this.addExperienceLevels(-1);
                this.expProgress = 1.0f + f / (float)this.getNextLevelExperience();
                continue;
            }
            this.addExperienceLevels(-1);
            this.expProgress = 0.0f;
        }
        while (this.expProgress >= 1.0f) {
            this.expProgress = (this.expProgress - 1.0f) * (float)this.getNextLevelExperience();
            this.addExperienceLevels(1);
            this.expProgress /= (float)this.getNextLevelExperience();
        }
    }

    public void addExperienceLevels(int levels) {
        this.expLevel += levels;
        if (this.expLevel < 0) {
            this.expLevel = 0;
            this.expProgress = 0.0f;
        }
    }

    public int getNextLevelExperience() {
        if (this.expLevel >= 30) {
            return 112 + (this.expLevel - 30) * 9;
        }
        if (this.expLevel >= 15) {
            return 37 + (this.expLevel - 15) * 5;
        }
        return 7 + this.expLevel * 2;
    }

    /**
     * Returns the title of this screen handler; will be a part of the open
     * screen packet sent to the client.
     */
    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new AutoAnvilScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(dir == Direction.UP || dir == Direction.DOWN){
            return slot == SLOT_ITEM_IN && getStack(slot).isEmpty();
        }

        if(stack.isOf(Items.EXPERIENCE_BOTTLE)) {
            return slot == SLOT_EXP_BOTTLE;
        }

        if(stack.isOf(Items.ANVIL) || stack.isOf(Items.CHIPPED_ANVIL) || stack.isOf(Items.DAMAGED_ANVIL)){
            return slot == SLOT_ANVIL && getStack(slot).isEmpty();
        }

        return slot == SLOT_ITEM_ADD;//a && getStack(slot).isEmpty();
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == SLOT_EMPTY_BOTTLE || slot == SLOT_ITEM_OUT;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < inventory.size(); i++){
            if(!inventory.get(i).isEmpty()){
                return false;
            }
        }

        return true;
    }

    /**
     * Fetches the stack currently stored at the given slot. If the slot is empty,
     * or is outside the bounds of this inventory, returns see {@link ItemStack#EMPTY}.
     *
     * @param slot
     */
    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    /**
     * Removes a specific number of items from the given slot.
     *
     * @param slot
     * @param amount
     * @return the removed items as a stack
     */
    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    /**
     * Removes the stack currently stored at the indicated slot.
     *
     * @param slot
     * @return the stack previously stored at the indicated slot.
     */
    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    public void dropXP(){
        int amt = this.expLevel * 7;
        amt = Math.min(amt, 100);
        ExperienceOrbEntity.spawn((ServerWorld) world, Vec3d.ofCenter(pos), amt);
    }
}
