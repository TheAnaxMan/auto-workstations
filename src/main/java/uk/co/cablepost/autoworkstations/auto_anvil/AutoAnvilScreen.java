package uk.co.cablepost.autoworkstations.auto_anvil;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.screen.ingame.BeaconScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

import java.util.List;

public class AutoAnvilScreen extends HandledScreen<AutoAnvilScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(AutoWorkstations.MOD_ID, "textures/gui/container/auto_anvil.png");

    public int lastXpEmptyProgress;

    public AutoAnvilScreen(AutoAnvilScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.titleY = 4;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexProgram);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //xp empty progress
        int xpEmptyProgress = handler.getXpEmptyProgress();
        drawTexture(matrices, x + 5, y + 42, 176, 55, 18, xpEmptyProgress);

        //xp bar progress
        int targetExpProgress = handler.getExpProgress();
        int expProgress = targetExpProgress;
        if(targetExpProgress > lastXpEmptyProgress){
            expProgress = Math.min(lastXpEmptyProgress + 2, targetExpProgress);
        }
        drawTexture(matrices, x + 37, y + 18 + (72 - expProgress), 180, 85, 6, expProgress);
        lastXpEmptyProgress = expProgress;

        //process progress
        int processProgress = handler.getProcessProgress();
        drawTexture(matrices, x + 115, y + 47, 176, 14, processProgress + 1, 16);

        //Mode Select
        textRenderer.draw(matrices, Text.translatable("gui.autoworkstations.auto_anvil.mode.repair"), x + 69, y + 23, 4210752);
        textRenderer.draw(matrices, Text.translatable("gui.autoworkstations.auto_anvil.mode.rename"), x + 118, y + 23, 4210752);

        int mode = handler.getSelectedMode();
        if(mode == AutoAnvilBlockEntity.MODE_REPAIR){
            textRenderer.draw(matrices, Text.of("x"), x + 60, y + 22, 0xffffff);
        }
        if(mode == AutoAnvilBlockEntity.MODE_RENAME){
            textRenderer.draw(matrices, Text.of("x"), x + 109, y + 22, 0xffffff);
        }

        //xp level
        String xpLevelStr = "" + handler.getExpLevel();
        int xpLevelTxtStart = 33 - xpLevelStr.length() * 6;
        textRenderer.drawWithShadow(matrices, Text.of(xpLevelStr), x + xpLevelTxtStart, y + 47, 0x33de00);

        //required xp level
        int requiredXpLevel = handler.getLevelCost();
        String requiredXpLevelStr = requiredXpLevel >= 40 ? "Too Expensive" : ("" + requiredXpLevel);
        int requiredXpLevelTxtStart = 81 - requiredXpLevelStr.length() * 3;
        textRenderer.drawWithShadow(matrices, Text.of(requiredXpLevelStr), x + requiredXpLevelTxtStart, y + 34, requiredXpLevel >= 40 ? 0xEE3333 : 0x33de00);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        super.render(matrices, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if(client == null || client.player == null || client.player.isSpectator()){
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        if(mouseY >= y + 23 && mouseY <= y + 29) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                PacketByteBuf buf = PacketByteBufs.create();
                buf.writeVarInt(AutoAnvilBlockEntity.MODE_REPAIR);
                ClientPlayNetworking.send(AutoWorkstations.UPDATE_AUTO_ANVIL_PACKET_ID, buf);
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);

                PacketByteBuf buf = PacketByteBufs.create();
                buf.writeVarInt(AutoAnvilBlockEntity.MODE_RENAME);
                ClientPlayNetworking.send(AutoWorkstations.UPDATE_AUTO_ANVIL_PACKET_ID, buf);
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    protected void drawForeground(MatrixStack matrices, int mouseX, int mouseY) {
        this.textRenderer.draw(matrices, this.title, (float)this.titleX, (float)this.titleY, 4210752);

        if(mouseY >= y + 23 && mouseY <= y + 29) {
            if (mouseX >= x + 59 && mouseX <= x + 66) {
                //Repair
                renderTooltip(
                        matrices,
                        Text.translatable("gui.autoworkstations.auto_anvil.mode.repair.tooltip"),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
            if (mouseX >= x + 108 && mouseX <= x + 115) {
                //Rename
                renderTooltip(
                        matrices,
                        List.of(
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_1"),
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_2"),
                                Text.translatable("gui.autoworkstations.auto_anvil.mode.rename.tooltip_3")
                        ),
                        mouseX - this.x,
                        mouseY - this.y
                );
            }
        }
    }
}
