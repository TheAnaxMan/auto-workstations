package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Items;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoEnchantingTableScreen extends HandledScreen<AutoEnchantingTableScreenHandler> {

    private static final Identifier TEXTURE = new Identifier(AutoWorkstations.MOD_ID, "textures/gui/container/auto_enchanting_table.png");

    public int lastXpEmptyProgress;

    public AutoEnchantingTableScreen(AutoEnchantingTableScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.playerInventoryTitleY = -999;//92;
        this.titleY = 4;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexProgram);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //xp empty progress
        int xpEmptyProgress = handler.getXpEmptyProgress();
        drawTexture(matrices, x + 5, y + 42, 176, 55, 18, xpEmptyProgress);

        //xp bar progress
        int targetExpProgress = handler.getExpProgress();
        int expProgress = targetExpProgress;
        if(targetExpProgress > lastXpEmptyProgress){
            expProgress = Math.min(lastXpEmptyProgress + 2, targetExpProgress);
        }
        drawTexture(matrices, x + 37, y + 18 + (72 - expProgress), 180, 85, 6, expProgress);
        lastXpEmptyProgress = expProgress;

        //enchant progress
        int enchantProgress = handler.getEnchantProgress();
        drawTexture(matrices, x + 100, y + 47, 176, 14, enchantProgress + 1, 16);

        //xp level
        String xpLevelStr = "" + handler.getExpLevel();
        int xpLevelTxtStart = 33 - xpLevelStr.length() * 6;
        textRenderer.drawWithShadow(matrices, Text.of(xpLevelStr), x + xpLevelTxtStart, y + 47, 0x33de00);

        //lapis count in slot
        int lapisCount = 0;
        int enchantTier = handler.getSelectedEnchantTier();
        if(handler.inventory.getStack(AutoEnchantingTableBlockEntity.SLOT_LAPIS).isOf(Items.LAPIS_LAZULI)){
            lapisCount = handler.inventory.getStack(AutoEnchantingTableBlockEntity.SLOT_LAPIS).getCount();
        }
        if(lapisCount > enchantTier){
            lapisCount = enchantTier;
        }
        textRenderer.draw(matrices, Text.of(lapisCount + "/" + enchantTier), x + 77, y + 65, 0x404040);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        super.render(matrices, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
}
