package uk.co.cablepost.autoworkstations.auto_enchanting_table.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlockEntity;

import java.util.Random;

public class IronAutoEnchantingTableBlockEntity extends AutoEnchantingTableBlockEntity {

    public IronAutoEnchantingTableBlockEntity(BlockPos pos, BlockState state) {
        super(AutoWorkstations.IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, pos, state);

        xpEmptyBottleMaxProgress = 20;
        maxEnchantProgress = 60;
    }
}
