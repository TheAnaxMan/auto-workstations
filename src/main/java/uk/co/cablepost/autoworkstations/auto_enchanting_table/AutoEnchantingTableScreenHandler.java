package uk.co.cablepost.autoworkstations.auto_enchanting_table;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoEnchantingTableScreenHandler extends ScreenHandler {

    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public AutoEnchantingTableScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(AutoEnchantingTableBlockEntity.INVENTORY_SIZE), new ArrayPropertyDelegate(AutoEnchantingTableBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    protected AutoEnchantingTableScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(AutoWorkstations.AUTO_ENCHANTING_TABLE_SCREEN_HANDLER, syncId);
        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        //some inventories do custom logic when a player opens it.
        inventory.onOpen(playerInventory.player);

        //Exp bottle
        this.addSlot(new Slot(inventory, AutoEnchantingTableBlockEntity.SLOT_EXP_BOTTLE, 16, 19));

        //Empty bottle
        this.addSlot(new Slot(inventory, AutoEnchantingTableBlockEntity.SLOT_EMPTY_BOTTLE, 16, 73));

        //Item to Enchant
        this.addSlot(new Slot(inventory, AutoEnchantingTableBlockEntity.SLOT_ITEM_TO_ENCHANT, 54, 46));

        //Lapis
        this.addSlot(new Slot(inventory, AutoEnchantingTableBlockEntity.SLOT_LAPIS, 78, 46));

        //Enchanted item
        this.addSlot(new Slot(inventory, AutoEnchantingTableBlockEntity.SLOT_ENCHANTED_ITEM, 133, 46));

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 102 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 160));
        }
    }

    // Shift + Player Inv Slot
    @Override
    public ItemStack quickMove(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    public int getXpEmptyProgress(){
        int xpEmptyBottleProgress = this.propertyDelegate.get(2);
        int xpEmptyBottleMaxProgress = this.propertyDelegate.get(3);

        if(xpEmptyBottleMaxProgress == 0){
            return 0;
        }

        return 26 * xpEmptyBottleProgress / xpEmptyBottleMaxProgress;
    }

    public int getExpProgress(){
        int expProgress100 = this.propertyDelegate.get(0);//from 0 to 100
        float value = (float)expProgress100 / 100f;//from 0 to 1
        return Math.round(value * 72f);
    }

    public int getExpLevel(){
        return this.propertyDelegate.get(1);
    }

    public int getEnchantProgress(){
        int enchantProgress = this.propertyDelegate.get(4);
        int enchantMaxProgress = this.propertyDelegate.get(5);

        if(enchantMaxProgress == 0){
            return 0;
        }

        return 24 * enchantProgress / enchantMaxProgress;
    }

    public int getSelectedEnchantTier(){
        return this.propertyDelegate.get(6);
    }
}
