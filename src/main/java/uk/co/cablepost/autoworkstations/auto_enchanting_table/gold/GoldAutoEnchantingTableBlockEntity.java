package uk.co.cablepost.autoworkstations.auto_enchanting_table.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlockEntity;

public class GoldAutoEnchantingTableBlockEntity extends AutoEnchantingTableBlockEntity {

    public GoldAutoEnchantingTableBlockEntity(BlockPos pos, BlockState state) {
        super(AutoWorkstations.GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, pos, state);

        xpEmptyBottleMaxProgress = 10;
        maxEnchantProgress = 30;
    }
}
