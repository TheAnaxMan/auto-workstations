package uk.co.cablepost.autoworkstations.auto_crafting_table;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.RecipeInputProvider;
import net.minecraft.recipe.RecipeMatcher;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.collection.DefaultedList;

public class AutoCraftingCraftingInventory extends CraftingInventory {

    private final DefaultedList<ItemStack> stacks2;
    private final int width2;
    private final int height2;

    public AutoCraftingCraftingInventory(int width, int height){
        super(null, 0, 0);
        this.stacks2 = DefaultedList.ofSize(width * height, ItemStack.EMPTY);
        this.width2 = width;
        this.height2 = height;
    }

    @Override
    public int size() {
        return this.stacks2.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : this.stacks2) {
            if (itemStack.isEmpty()) continue;
            return false;
        }
        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        if (slot >= this.size()) {
            return ItemStack.EMPTY;
        }
        return this.stacks2.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot) {
        return Inventories.removeStack(this.stacks2, slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        ItemStack itemStack = Inventories.splitStack(this.stacks2, slot, amount);
        return itemStack;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        this.stacks2.set(slot, stack);
    }

    @Override
    public void markDirty() {
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        this.stacks2.clear();
    }

    public int getHeight() {
        return this.height2;
    }

    public int getWidth() {
        return this.width2;
    }

    @Override
    public void provideRecipeInputs(RecipeMatcher finder) {
        for (ItemStack itemStack : this.stacks2) {
            finder.addUnenchantedInput(itemStack);
        }
    }
}
