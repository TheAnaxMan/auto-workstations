package uk.co.cablepost.autoworkstations.auto_crafting_table.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntity;

public class IronAutoCraftingTableBlockEntity extends AutoCraftingTableBlockEntity {
    public IronAutoCraftingTableBlockEntity(BlockPos blockPos, BlockState blockState) {
        super(AutoWorkstations.IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, blockPos, blockState);
        this.ANIM_SPEED = 0.05f;
    }
}
