package uk.co.cablepost.autoworkstations.auto_crafting_table;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class AutoCraftingTableBlockEntity extends BlockEntity implements NamedScreenHandlerFactory, SidedInventory, Inventory {
    public DefaultedList<ItemStack> inventory = DefaultedList.ofSize(27, ItemStack.EMPTY);
    public int forceCheckCanCraftTimer = 20 * 3;
    public int[] slotActuallyHasItem = new int[size()];
    public CraftingRecipe craftingRecipe;
    public AutoCraftingCraftingInventory craftingInventory;

    DefaultedList<ItemStack> craftingOutput = null;

    public static int UNSET_STATE = 0;
    public static int NO_RECIPE_STATE = 1;
    public static int NOT_ENOUGH_RESOURCES_STATE = 2;
    public static int OUTPUT_FULL_STATE = 3;
    public static int WORKING_STATE = 4;
    public static int RECIPE_LOCKED_STATE = 5;

    public int state = UNSET_STATE;

    public float ANIM_SPEED = 0.01f;
    public int BATCH_CRAFT_COUNT = 1;
    public static float ANIM_CORNER_X = 0.1f;
    public static float ANIM_CORNER_Z = 0.9f;
    public static float ANIM_SLOT_START_X = 5f / 16f;
    public static float ANIM_SLOT_START_Z = 5f / 16f;
    public static float ANIM_SLOT_GAP_X = 3f / 16f;
    public static float ANIM_SLOT_GAP_Z = 3f / 16f;

    public int nextAnimSlot = 0;
    public boolean animToCorner = true;
    public float animX = ANIM_CORNER_X;
    public float animZ = ANIM_CORNER_Z;


    public static int PATTERN_SLOT_START = 0;
    public static int PATTERN_SLOT_END = 8;
    public static int INPUT_SLOT_START = 9;
    public static int INPUT_SLOT_END = 17;
    public static int OUTPUT_SLOT_START = 18;
    public static int OUTPUT_SLOT_END = 26;

    public static UUID emptyUUID = UUID.fromString("e7459a01-125e-43c5-bd89-58950fa43e4d");
    public UUID lastPlayerUUID;
    public boolean lastRecipeUnlockedRes;

    // --- Constructor ---

    public AutoCraftingTableBlockEntity(BlockEntityType blockEntityType, BlockPos blockPos, BlockState blockState) {
        super(blockEntityType, blockPos, blockState);
        lastPlayerUUID = emptyUUID;
        craftingInventory = new AutoCraftingCraftingInventory(3, 3);
    }

    // --- Network sync ---

    @Override
    public void writeNbt(NbtCompound tag) {
        Inventories.writeNbt(tag, inventory);
        tag.putIntArray("SlotActuallyHasItem", slotActuallyHasItem);
        tag.putBoolean("AnimToCorner", animToCorner);
        tag.putInt("NextAnimSlot", nextAnimSlot);
        tag.putInt("State", state);
        tag.putUuid("LastPlayerUUID", lastPlayerUUID);
        tag.putBoolean("LastRecipeUnlockedRes", lastRecipeUnlockedRes);

        super.writeNbt(tag);

    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);

        Inventories.readNbt(tag, inventory);
        slotActuallyHasItem = tag.getIntArray("SlotActuallyHasItem");
        animToCorner = tag.getBoolean("AnimToCorner");
        nextAnimSlot = tag.getInt("NextAnimSlot");
        state = tag.getInt("State");
        try {
            lastPlayerUUID = tag.getUuid("LastPlayerUUID");
        }
        catch(Exception e) {
            lastPlayerUUID = emptyUUID;
        }
        lastRecipeUnlockedRes = tag.getBoolean("LastRecipeUnlockedRes");
    }

    public static int PROPERTY_DELEGATE_SIZE = 1;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            if (index == 0) {
                return Math.round(AutoCraftingTableBlockEntity.this.state);
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }


    // --- Screen and inventory ---

    /**
     * Returns the title of this screen handler; will be a part of the open
     * screen packet sent to the client.
     */
    @Override
    public Text getDisplayName() {
        return translatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        lastPlayerUUID = player.getUuid();
        return new AutoCraftingTableScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        return inventory
                .stream()
                .filter(ItemStack::isEmpty)
                .toArray()
                .length > 0;
    }

    /**
     * Fetches the stack currently stored at the given slot. If the slot is empty,
     * or is outside the bounds of this inventory, returns see {@link ItemStack#EMPTY}.
     *
     * @param slot
     */
    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    /**
     * Removes a specific number of items from the given slot.
     *
     * @param slot
     * @param amount
     * @return the removed items as a stack
     */
    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    /**
     * Removes the stack currently stored at the indicated slot.
     *
     * @param slot
     * @return the stack previously stored at the indicated slot.
     */
    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;

    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    public void checkInput(){
        updateSlotActuallyEmptyHack();
        updateCraftingRecipe();
        updateCanCraft();

        if(craftingOutput != null && !spaceInOutput(craftingOutput)){
            state = OUTPUT_FULL_STATE;
        }
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        if(serverSide){
            checkInput();
        }

        super.markDirty();

        if (serverSide) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    public void updateSlotActuallyEmptyHack(){
        for(int i = 0; i < size(); i++){
            slotActuallyHasItem[i] = getStack(i).isEmpty() ? 0 : 1;
        }
    }


    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    /**
     * Gets the available slot positions that are reachable from a given side.
     *
     * @param side
     */
    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    /**
     * Determines whether the given stack can be inserted into this inventory at the specified slot position from the given direction.
     *
     * @param slot
     * @param stack
     * @param dir
     */
    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(slot < INPUT_SLOT_START || slot > INPUT_SLOT_END){
            return false;
        }

        Item item = stack.getItem();

        int countInInput = countItemInInput(item);
        int maxStackSize = stack.getMaxCount();
        if(
                countInInput >= countPatternSlotsWithItem(item) * maxStackSize//if 2 slots in the pattern are of this item type, only allow 2 stacks worth of items to go into the input via hoppers
                ||
                (countInInput % maxStackSize != 0 && getStack(slot).isEmpty())//if trying to fill into a new slot when another slot has not been fully filled yet
        ){
            return false;
        }

        return true;
    }

    /**
     * Determines whether the given stack can be removed from this inventory at the specified slot position from the given direction.
     *
     * @param slot
     * @param stack
     * @param dir
     */
    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot >= OUTPUT_SLOT_START && slot <= OUTPUT_SLOT_END;
    }

    @Override
    public void clear() {
        inventory.clear();
    }


    // --- Tick logic ---

    public static void clientTick(World world, BlockPos pos, BlockState state, AutoCraftingTableBlockEntity blockEntity) {
        blockEntity.updateAnim();
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, AutoCraftingTableBlockEntity blockEntity) {
        if(blockEntity.updateAnim()){
            for(int i = 1; i <= blockEntity.BATCH_CRAFT_COUNT; i++){
                if(!blockEntity.tryCraft()){
                    break;
                }
            }
        }

        blockEntity.forceCheckCanCraftTimer -= 1;
        if(blockEntity.forceCheckCanCraftTimer <= 0){
            blockEntity.forceCheckCanCraftTimer = 20 * 5;
            blockEntity.checkInput();
        }
    }

    // --- crafting logic ---

    boolean updateCraftingInventory(){
        boolean changed = false;

        for(int i = 0; i < 9; i++){
            ItemStack beforeStack = craftingInventory.getStack(i).copy();
            craftingInventory.setStack(i, getStack(i + PATTERN_SLOT_START).copy());
            ItemStack afterStack = craftingInventory.getStack(i).copy();
            if((!beforeStack.isItemEqual(afterStack)) && (beforeStack.isEmpty() != afterStack.isEmpty())){
                changed = true;
            }
        }

        return changed;
    }

    void updateCraftingRecipe() {
        if(!updateCraftingInventory()){
            return;
        }

        String recipeIdBefore = "";
        if(craftingRecipe != null){
            recipeIdBefore = craftingRecipe.getId().toString();
        }

        assert world != null;
        Optional<CraftingRecipe> optional = Objects.requireNonNull(world.getServer()).getRecipeManager().getFirstMatch(
                RecipeType.CRAFTING,
                craftingInventory,
                world
        );

        if (optional.isEmpty()) {
            craftingRecipe = null;
            return;
        }

        craftingRecipe = optional.get();

        if(!Objects.equals(craftingRecipe.getId().toString(), recipeIdBefore)){
            updateCraftingOutput();
        }
    }

    public boolean recipeLocked(){
        if(world == null){
            return true;
        }

        if(!world.getGameRules().getBoolean(GameRules.DO_LIMITED_CRAFTING)){
            return false;
        }

        if(lastPlayerUUID.equals(emptyUUID)){
            return true;
        }

        ServerPlayerEntity serverPlayerEntity =
                Objects.requireNonNull(world.getServer())
                        .getPlayerManager()
                        .getPlayer(lastPlayerUUID)
        ;

        if(serverPlayerEntity != null){
            lastRecipeUnlockedRes = serverPlayerEntity.getRecipeBook().contains(craftingRecipe);
        }

        return !lastRecipeUnlockedRes;
    }

    public boolean haveEnoughItemsToCraft(){
        for(int i = PATTERN_SLOT_START; i <= PATTERN_SLOT_END; i++){
            if(inventory.get(i).isEmpty()){
                continue;
            }
            Item item = inventory.get(i).getItem();
            if(countPatternSlotsWithItem(item) > countItemInInput(item)){
                return false;
            }
        }

        return true;
    }

    public void takeMaterialsFromInput(){
        for(int i = PATTERN_SLOT_START; i <= PATTERN_SLOT_END; i++){
            if(inventory.get(i).isEmpty()){
                continue;
            }
            Item requiredItem = inventory.get(i).getItem();
            for(int j = INPUT_SLOT_START; j <= INPUT_SLOT_END; j++){
                ItemStack inputStack = getStack(j);
                if(inputStack.isOf(requiredItem)){
                    inputStack.decrement(1);
                    break;
                }
            }
        }
    }

    public boolean spaceInOutput(DefaultedList<ItemStack> toAdd2){
        //copy toAdd stacks to temp location to test on
        DefaultedList<ItemStack> toAdd = DefaultedList.ofSize(toAdd2.size(), ItemStack.EMPTY);

        for(int i = 0; i < toAdd.size(); i++){
            toAdd.set(i, toAdd2.get(i).copy());
        }

        //copy output stacks to temp location to test on
        DefaultedList<ItemStack> output = DefaultedList.ofSize(9, ItemStack.EMPTY);

        for(int i = OUTPUT_SLOT_START; i <= OUTPUT_SLOT_END; i++){
            output.set(i - OUTPUT_SLOT_START, getStack(i).copy());
        }

        //try inserting all stacks

        for(int i = 0; i < toAdd.size(); i++){
            ItemStack toAddItemStack = toAdd.get(i);
            if(toAddItemStack.isEmpty()){
                continue;
            }

            for(int j = 0; j < output.size(); j++){
                ItemStack outputItemStack = output.get(j);

                if(!outputItemStack.isEmpty() && !ItemStack.canCombine(toAddItemStack, outputItemStack)){
                    continue;
                }

                int spaceLeftInOutputSlot = toAddItemStack.getMaxCount();
                if(!outputItemStack.isEmpty()){
                    spaceLeftInOutputSlot = outputItemStack.getMaxCount() - outputItemStack.getCount();
                }

                int canAddHere = Math.min(toAddItemStack.getCount(), spaceLeftInOutputSlot);

                if(outputItemStack.isEmpty()){
                    output.set(j, toAddItemStack.copy());
                }
                else{
                    outputItemStack.increment(canAddHere);
                }

                toAddItemStack.decrement(canAddHere);

                if(toAddItemStack.isEmpty()){
                    break;
                }
            }

            if(!toAddItemStack.isEmpty()){
                return false;
            }
        }

        return true;
    }

    private void addItemToOutput(ItemStack itemStack){
        //assumes spaceInOutput has been run and that this can be inserted

        for(int i = OUTPUT_SLOT_START; i <= OUTPUT_SLOT_END; i++){
            ItemStack outputItemStack = getStack(i);

            if(!outputItemStack.isEmpty() && !ItemStack.canCombine(itemStack, outputItemStack)){
                continue;
            }

            int spaceLeftInOutputSlot = itemStack.getMaxCount();
            if(!outputItemStack.isEmpty()){
                spaceLeftInOutputSlot = outputItemStack.getMaxCount() - outputItemStack.getCount();
            }

            int canAddHere = Math.min(itemStack.getCount(), spaceLeftInOutputSlot);

            if(outputItemStack.isEmpty()){
                setStack(i, itemStack.copy());
            }
            else{
                outputItemStack.increment(canAddHere);
            }

            itemStack.decrement(canAddHere);

            if(itemStack.isEmpty()){
                break;
            }
        }
    }

    boolean addItemsToOutput(DefaultedList<ItemStack> toAdd){
        if(!spaceInOutput(toAdd)){
            return false;
        }


        for(int i = 0; i < toAdd.size(); i++){
            ItemStack toAddItemStack = toAdd.get(i).copy();
            if(toAddItemStack.isEmpty()){
                continue;
            }

            addItemToOutput(toAddItemStack);
        }


        return true;
    }

    void updateCanCraft(){
        if(craftingRecipe == null){
            state = NO_RECIPE_STATE;
            return;
        }

        if(recipeLocked()){
            state = RECIPE_LOCKED_STATE;
            return;
        }

        if(!haveEnoughItemsToCraft()){
            state = NOT_ENOUGH_RESOURCES_STATE;
            return;
        }

        state = WORKING_STATE;
    }

    public boolean tryCraft(){
        if(craftingRecipe == null){
            state = NO_RECIPE_STATE;
            return false;
        }

        if(recipeLocked()){
            state = RECIPE_LOCKED_STATE;
            return false;
        }

        if(!haveEnoughItemsToCraft()){
            state = NOT_ENOUGH_RESOURCES_STATE;
            return false;
        }

        if(!addItemsToOutput(craftingOutput)){
            state = OUTPUT_FULL_STATE;
            return false;
        }


        takeMaterialsFromInput();
        return true;
    }

    private void updateCraftingOutput(){
        if(craftingRecipe == null){
            craftingOutput = null;
            return;
        }

        ItemStack craftingResult = craftingRecipe.craft(craftingInventory);
        DefaultedList<ItemStack> remainder = craftingRecipe.getRemainder(craftingInventory);

        DefaultedList<ItemStack> toAdd = DefaultedList.ofSize(10, ItemStack.EMPTY);
        for(int i = 0; i < remainder.size(); i++){
            toAdd.set(i, remainder.get(i).copy());
        }
        toAdd.set(9, craftingResult.copy());

        craftingOutput = toAdd;
    }

    public int countPatternSlotsWithItem(Item item){
        int total = 0;

        for(int i = PATTERN_SLOT_START; i <= PATTERN_SLOT_END; i++){
            ItemStack invStack = inventory.get(i);
            if(invStack.isOf(item)){
                total += 1;
            }
        }

        return total;
    }

    public int countItemInInput(Item item){
        int total = 0;

        for(int i = INPUT_SLOT_START; i <= INPUT_SLOT_END; i++){
            ItemStack invStack = inventory.get(i);
            if(invStack.isOf(item)){
                total += invStack.getCount();
            }
        }

        return total;
    }

    // --- Animation ---

    public boolean updateAnim(){
        if(Objects.equals(state, WORKING_STATE)){
            if(updateAnim2()){//anim complete
                nextAnimSlot = -1;
                animToCorner = false;
                return true;
            }
        }
        else{
            nextAnimSlot = -1;
            animToCorner = false;
        }

        return false;
    }

    public boolean updateAnim2(){//returns true when craft anim complete
        boolean serverSide = this.hasWorld() && !this.getWorld().isClient();

        if(animToCorner){
            if(moveAnimTowardsCords(ANIM_CORNER_X, ANIM_CORNER_Z)){
                animToCorner = false;
                if(serverSide) {
                    world.playSound(null, pos, SoundEvents.BLOCK_BEEHIVE_EXIT, SoundCategory.BLOCKS, 0.03f, world.random.nextFloat() * 0.1f + 0.9f);
                }

                for(int i = nextAnimSlot + 1; i < 9; i++){
                    ItemStack patternStack = getStack(i + PATTERN_SLOT_START);
                    if(!patternStack.isEmpty()){
                        nextAnimSlot = i;
                        markDirty();
                        return false;
                    }
                }

                nextAnimSlot = -1;
                markDirty();
                return true;
            }
        }
        else {
            if (moveAnimTowardsCords(
                    ANIM_SLOT_START_X + (nextAnimSlot % 3) * ANIM_SLOT_GAP_X,
                    ANIM_SLOT_START_Z + ((float) Math.floor(nextAnimSlot / 3f) * ANIM_SLOT_GAP_Z)
            )) {
                animToCorner = true;
                if (serverSide) {
                    world.playSound(null, pos, SoundEvents.BLOCK_METAL_PLACE, SoundCategory.BLOCKS, 0.12f, world.random.nextFloat() * 0.1f + 0.9f);
                }
                markDirty();
            }
        }
        return false;
    }

    public boolean moveAnimTowardsCords(float tx, float tz){
        if(nextAnimSlot == -1 && !animToCorner){
            return true;
        }

        boolean toRet = true;

        if(Math.abs(animX - tx) < ANIM_SPEED){
            animX = tx;
        }

        if(animX > tx){
            animX -= ANIM_SPEED;
        }
        if(animX < tx){
            animX += ANIM_SPEED;
        }

        if(animX != tx){
            toRet = false;
        }


        if(Math.abs(animZ - tz) < ANIM_SPEED){
            animZ = tz;
        }

        if(animZ > tz){
            animZ -= ANIM_SPEED;
        }
        if(animZ < tz){
            animZ += ANIM_SPEED;
        }

        if(animZ != tz){
            toRet = false;
        }

        return toRet;
    }
}
