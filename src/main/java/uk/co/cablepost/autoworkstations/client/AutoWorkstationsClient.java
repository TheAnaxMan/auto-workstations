package uk.co.cablepost.autoworkstations.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilScreen;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandScreen;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableScreen;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlockEntityRenderer;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableScreen;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterScreen;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumScreen;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceScreen;

@Environment(EnvType.CLIENT)
public class AutoWorkstationsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

        // -- auto crafting table ---

        BlockEntityRendererRegistry.register(AutoWorkstations.IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(AutoWorkstations.GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(AutoWorkstations.NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY, AutoCraftingTableBlockEntityRenderer::new);

        ScreenRegistry.register(AutoWorkstations.AUTO_CRAFTING_TABLE_SCREEN_HANDLER, AutoCraftingTableScreen::new);

        // --- auto furnace ---

        HandledScreens.register(AutoWorkstations.AUTO_FURNACE_SCREEN_HANDLER, AutoFurnaceScreen::new);

        // --- auto enchanting table ---

        HandledScreens.register(AutoWorkstations.AUTO_ENCHANTING_TABLE_SCREEN_HANDLER, AutoEnchantingTableScreen::new);

        BlockRenderLayerMap.INSTANCE.putBlock(AutoWorkstations.IRON_AUTO_ENCHANTING_TABLE_BLOCK, RenderLayer.getTranslucent());
        BlockRenderLayerMap.INSTANCE.putBlock(AutoWorkstations.GOLD_AUTO_ENCHANTING_TABLE_BLOCK, RenderLayer.getTranslucent());

        BlockEntityRendererRegistry.register(AutoWorkstations.IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, AutoEnchantingTableBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(AutoWorkstations.GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY, AutoEnchantingTableBlockEntityRenderer::new);

        BlockRenderLayerMap.INSTANCE.putBlock(AutoWorkstations.AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK, RenderLayer.getTranslucent());

        // --- auto experience orb vacuum ---

        HandledScreens.register(AutoWorkstations.AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER, AutoExperienceOrbVacuumScreen::new);

        // --- auto experience orb emitter ---

        HandledScreens.register(AutoWorkstations.AUTO_EXPERIENCE_ORB_EMITTER_SCREEN_HANDLER, AutoExperienceOrbEmitterScreen::new);

        // --- auto anvil ---

        BlockEntityRendererRegistry.register(AutoWorkstations.IRON_AUTO_ANVIL_BLOCK_ENTITY, AutoAnvilBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(AutoWorkstations.GOLD_AUTO_ANVIL_BLOCK_ENTITY, AutoAnvilBlockEntityRenderer::new);

        HandledScreens.register(AutoWorkstations.AUTO_ANVIL_SCREEN_HANDLER, AutoAnvilScreen::new);

        // --- auto brewing stand ---

        HandledScreens.register(AutoWorkstations.AUTO_BREWING_STAND_SCREEN_HANDLER, AutoBrewingStandScreen::new);

        BlockRenderLayerMap.INSTANCE.putBlock(AutoWorkstations.IRON_AUTO_BREWING_STAND_BLOCK, RenderLayer.getTranslucent());
        BlockRenderLayerMap.INSTANCE.putBlock(AutoWorkstations.GOLD_AUTO_BREWING_STAND_BLOCK, RenderLayer.getTranslucent());
    }
}