package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumBlock;

import java.util.List;

import static uk.co.cablepost.autoworkstations.util.TextUtil.translatableText;

public class GoldAutoExperienceOrbVacuumBlock extends AutoExperienceOrbVacuumBlock {
    public GoldAutoExperienceOrbVacuumBlock(Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldAutoExperienceOrbVacuumBlockEntity(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                GoldAutoExperienceOrbVacuumBlock.checkType(type, AutoWorkstations.GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, GoldAutoExperienceOrbVacuumBlockEntity::clientTick) :
                GoldAutoExperienceOrbVacuumBlock.checkType(type, AutoWorkstations.GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY, GoldAutoExperienceOrbVacuumBlockEntity::serverTick)
        ;
    }

    @Override
    public void appendTooltip(ItemStack itemStack, BlockView world, List<Text> tooltip, TooltipContext tooltipContext) {
        tooltip.add( translatableText("block.autoworkstations.gold_auto_experience_orb_vacuum.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
        tooltip.add( translatableText("block.autoworkstations.hopper_compatible.tooltip").formatted(Formatting.ITALIC, Formatting.DARK_PURPLE) );
    }
}
