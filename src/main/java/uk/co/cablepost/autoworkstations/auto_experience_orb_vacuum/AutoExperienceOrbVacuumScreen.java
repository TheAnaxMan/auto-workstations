package uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.AutoWorkstations;

public class AutoExperienceOrbVacuumScreen extends HandledScreen<AutoExperienceOrbVacuumScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(AutoWorkstations.MOD_ID, "textures/gui/container/auto_experience_orb_vacuum.png");

    public AutoExperienceOrbVacuumScreen(AutoExperienceOrbVacuumScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        this.backgroundWidth = 176;
        this.backgroundHeight = 184;
        this.playerInventoryTitleY = 92;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexProgram);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        int xpFillProgress = handler.getXpFillProgress();
        drawTexture(matrices, x + 89, y + 41, 176, 55, 18, xpFillProgress);

        float bottlesCanFill = handler.getXpBottlesCouldFill();
        textRenderer.draw(matrices, Text.of("x" + bottlesCanFill), x + 82, y + 50, 0x404040);
        textRenderer.draw(matrices, Text.of("x" + bottlesCanFill), x + 81, y + 49, 0x33de00);
    }

    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}
