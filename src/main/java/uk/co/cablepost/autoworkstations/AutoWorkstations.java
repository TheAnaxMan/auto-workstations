package uk.co.cablepost.autoworkstations;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.AutoAnvilScreenHandler;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.gold.GoldAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlock;
import uk.co.cablepost.autoworkstations.auto_anvil.iron.IronAutoAnvilBlockEntity;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandScreenHandler;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.gold.GoldAutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.gold.GoldAutoBrewingStandBlockEntity;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.iron.IronAutoBrewingStandBlock;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.iron.IronAutoBrewingStandBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.AutoCraftingTableScreenHandler;
import uk.co.cablepost.autoworkstations.auto_crafting_table.gold.GoldAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.gold.GoldAutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.iron.IronAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.iron.IronAutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlock;
import uk.co.cablepost.autoworkstations.auto_crafting_table.netherite.NetheriteAutoCraftingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableScreenHandler;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.AutoEnchantingTableXpInsideBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.gold.GoldAutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.gold.GoldAutoEnchantingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.iron.IronAutoEnchantingTableBlock;
import uk.co.cablepost.autoworkstations.auto_enchanting_table.iron.IronAutoEnchantingTableBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.AutoExperienceOrbEmitterScreenHandler;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.gold.GoldAutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.gold.GoldAutoExperienceOrbEmitterBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron.IronAutoExperienceOrbEmitterBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_emitter.iron.IronAutoExperienceOrbEmitterBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.AutoExperienceOrbVacuumScreenHandler;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold.GoldAutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.gold.GoldAutoExperienceOrbVacuumBlockEntity;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron.IronAutoExperienceOrbVacuumBlock;
import uk.co.cablepost.autoworkstations.auto_experience_orb_vacuum.iron.IronAutoExperienceOrbVacuumBlockEntity;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.AutoFurnaceScreenHandler;
import uk.co.cablepost.autoworkstations.auto_furnace.gold.GoldAutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.gold.GoldAutoFurnaceBlockEntity;
import uk.co.cablepost.autoworkstations.auto_furnace.iron.IronAutoFurnaceBlock;
import uk.co.cablepost.autoworkstations.auto_furnace.iron.IronAutoFurnaceBlockEntity;

public class AutoWorkstations implements ModInitializer {

    public static final String MOD_ID = "autoworkstations";

    public static final float EXPERIENCE_BOTTLE_VALUE = 8.0f;
    public static final Identifier UPDATE_AUTO_ANVIL_PACKET_ID = new Identifier(MOD_ID, "update_auto_anvil");

    // --- auto crafting table ---

    //screen
    public static final Identifier AUTO_CRAFTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "auto_crafting_table");
    public static final ScreenHandlerType<AutoCraftingTableScreenHandler> AUTO_CRAFTING_TABLE_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_CRAFTING_TABLE_IDENTIFIER, AutoCraftingTableScreenHandler::new);

    //iron
    public static AutoCraftingTableBlock IRON_AUTO_CRAFTING_TABLE_BLOCK = new IronAutoCraftingTableBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_CRAFTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_crafting_table");
    public static BlockEntityType<IronAutoCraftingTableBlockEntity> IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    //gold
    public static AutoCraftingTableBlock GOLD_AUTO_CRAFTING_TABLE_BLOCK = new GoldAutoCraftingTableBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_crafting_table");
    public static BlockEntityType<GoldAutoCraftingTableBlockEntity> GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    //netherite
    public static AutoCraftingTableBlock NETHERITE_AUTO_CRAFTING_TABLE_BLOCK = new NetheriteAutoCraftingTableBlock(FabricBlockSettings.of(Material.METAL).strength(3.0f));
    public static final Identifier NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "netherite_auto_crafting_table");
    public static BlockEntityType<NetheriteAutoCraftingTableBlockEntity> NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY;

    // --- auto furnace ---

    //screen
    public static final Identifier AUTO_FURNACE_IDENTIFIER = new Identifier(MOD_ID, "auto_furnace");
    public static final ScreenHandlerType<AutoFurnaceScreenHandler> AUTO_FURNACE_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_FURNACE_IDENTIFIER, AutoFurnaceScreenHandler::new);

    //iron
    public static AutoFurnaceBlock IRON_AUTO_FURNACE_BLOCK = new IronAutoFurnaceBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_FURNACE_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_furnace");
    public static BlockEntityType<IronAutoFurnaceBlockEntity> IRON_AUTO_FURNACE_BLOCK_ENTITY;

    //gold
    public static AutoFurnaceBlock GOLD_AUTO_FURNACE_BLOCK = new GoldAutoFurnaceBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_FURNACE_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_furnace");
    public static BlockEntityType<GoldAutoFurnaceBlockEntity> GOLD_AUTO_FURNACE_BLOCK_ENTITY;

    // --- auto enchanting table ---

    //screen
    public static final Identifier AUTO_ENCHANTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "auto_enchanting_table");
    public static final ScreenHandlerType<AutoEnchantingTableScreenHandler> AUTO_ENCHANTING_TABLE_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_ENCHANTING_TABLE_IDENTIFIER, AutoEnchantingTableScreenHandler::new);

    //xp inside
    public static final Identifier AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER = new Identifier(MOD_ID, "auto_enchanting_table_xp_inside");
    public static AutoEnchantingTableXpInsideBlock AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK = new AutoEnchantingTableXpInsideBlock(FabricBlockSettings.of(Material.AMETHYST).strength(999f).nonOpaque().luminance(10));

    //iron
    public static AutoEnchantingTableBlock IRON_AUTO_ENCHANTING_TABLE_BLOCK = new IronAutoEnchantingTableBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f).nonOpaque().luminance(8));
    public static final Identifier IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_enchanting_table");
    public static BlockEntityType<IronAutoEnchantingTableBlockEntity> IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY;

    //gold
    public static AutoEnchantingTableBlock GOLD_AUTO_ENCHANTING_TABLE_BLOCK = new GoldAutoEnchantingTableBlock(FabricBlockSettings.of(Material.METAL).strength(1.5f).nonOpaque().luminance(8));
    public static final Identifier GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_enchanting_table");
    public static BlockEntityType<GoldAutoEnchantingTableBlockEntity> GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY;

    // --- auto experience orb vacuum ---

    public static final Identifier AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = new Identifier(MOD_ID, "auto_experience_orb_vacuum");
    public static final ScreenHandlerType<AutoExperienceOrbVacuumScreenHandler> AUTO_EXPERIENCE_ORB_VACUUM_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, AutoExperienceOrbVacuumScreenHandler::new);

    //iron
    public static AutoExperienceOrbVacuumBlock IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK = new IronAutoExperienceOrbVacuumBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_experience_orb_vacuum");
    public static BlockEntityType<IronAutoExperienceOrbVacuumBlockEntity> IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY;

    //gold
    public static AutoExperienceOrbVacuumBlock GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK = new GoldAutoExperienceOrbVacuumBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_experience_orb_vacuum");
    public static BlockEntityType<GoldAutoExperienceOrbVacuumBlockEntity> GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY;

    // --- auto experience orb emitter ---

    public static final Identifier AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = new Identifier(MOD_ID, "auto_experience_orb_emitter");
    public static final ScreenHandlerType<AutoExperienceOrbEmitterScreenHandler> AUTO_EXPERIENCE_ORB_EMITTER_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, AutoExperienceOrbEmitterScreenHandler::new);

    //iron
    public static AutoExperienceOrbEmitterBlock IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK = new IronAutoExperienceOrbEmitterBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_experience_orb_emitter");
    public static BlockEntityType<IronAutoExperienceOrbEmitterBlockEntity> IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY;

    //gold
    public static AutoExperienceOrbEmitterBlock GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK = new GoldAutoExperienceOrbEmitterBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_experience_orb_emitter");
    public static BlockEntityType<GoldAutoExperienceOrbEmitterBlockEntity> GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY;

    // --- auto anvil ---

    //screen
    public static final Identifier AUTO_ANVIL_IDENTIFIER = new Identifier(MOD_ID, "auto_anvil");
    public static final ScreenHandlerType<AutoAnvilScreenHandler> AUTO_ANVIL_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_ANVIL_IDENTIFIER, AutoAnvilScreenHandler::new);

    //iron
    public static AutoAnvilBlock IRON_AUTO_ANVIL_BLOCK = new IronAutoAnvilBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_ANVIL_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_anvil");
    public static BlockEntityType<IronAutoAnvilBlockEntity> IRON_AUTO_ANVIL_BLOCK_ENTITY;

    //gold
    public static AutoAnvilBlock GOLD_AUTO_ANVIL_BLOCK = new GoldAutoAnvilBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_ANVIL_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_anvil");
    public static BlockEntityType<GoldAutoAnvilBlockEntity> GOLD_AUTO_ANVIL_BLOCK_ENTITY;

    // --- auto brewing stand ---

    //screen
    public static final Identifier AUTO_BREWING_STAND_IDENTIFIER = new Identifier(MOD_ID, "auto_brewing_stand");
    public static final ScreenHandlerType<AutoBrewingStandScreenHandler> AUTO_BREWING_STAND_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(AUTO_BREWING_STAND_IDENTIFIER, AutoBrewingStandScreenHandler::new);

    //iron
    public static AutoBrewingStandBlock IRON_AUTO_BREWING_STAND_BLOCK = new IronAutoBrewingStandBlock(FabricBlockSettings.of(Material.METAL).strength(2.0f));
    public static final Identifier IRON_AUTO_BREWING_STAND_IDENTIFIER = new Identifier(MOD_ID, "iron_auto_brewing_stand");
    public static BlockEntityType<IronAutoBrewingStandBlockEntity> IRON_AUTO_BREWING_STAND_BLOCK_ENTITY;

    //gold
    public static AutoBrewingStandBlock GOLD_AUTO_BREWING_STAND_BLOCK = new GoldAutoBrewingStandBlock(FabricBlockSettings.of(Material.METAL).strength(1.0f));
    public static final Identifier GOLD_AUTO_BREWING_STAND_IDENTIFIER = new Identifier(MOD_ID, "gold_auto_brewing_stand");
    public static BlockEntityType<GoldAutoBrewingStandBlockEntity> GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY;

    // --- item group ---

    public static final ItemGroup ITEM_GROUP = FabricItemGroup.builder(
            new Identifier(MOD_ID, "items")
    ).icon(() -> new ItemStack(GOLD_AUTO_CRAFTING_TABLE_BLOCK)).build();

    @Override
    public void onInitialize() {
        // --- auto crafting table ---

        //iron

        IRON_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoCraftingTableBlockEntity::new,
                        IRON_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_CRAFTING_TABLE_IDENTIFIER, IRON_AUTO_CRAFTING_TABLE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_CRAFTING_TABLE_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(IRON_AUTO_CRAFTING_TABLE_BLOCK, new FabricItemSettings()));

        //gold

        GOLD_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoCraftingTableBlockEntity::new,
                        GOLD_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER, GOLD_AUTO_CRAFTING_TABLE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_CRAFTING_TABLE_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(GOLD_AUTO_CRAFTING_TABLE_BLOCK, new FabricItemSettings()));

        //netherite

        NETHERITE_AUTO_CRAFTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteAutoCraftingTableBlockEntity::new,
                        NETHERITE_AUTO_CRAFTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER, NETHERITE_AUTO_CRAFTING_TABLE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(NETHERITE_AUTO_CRAFTING_TABLE_BLOCK));
        Registry.register(Registries.ITEM, NETHERITE_AUTO_CRAFTING_TABLE_IDENTIFIER, new BlockItem(NETHERITE_AUTO_CRAFTING_TABLE_BLOCK, new FabricItemSettings()));

        // --- auto furnace ---

        //iron

        IRON_AUTO_FURNACE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_FURNACE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoFurnaceBlockEntity::new,
                        IRON_AUTO_FURNACE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_FURNACE_IDENTIFIER, IRON_AUTO_FURNACE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_FURNACE_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_FURNACE_IDENTIFIER, new BlockItem(IRON_AUTO_FURNACE_BLOCK, new FabricItemSettings()));

        //gold

        GOLD_AUTO_FURNACE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_FURNACE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoFurnaceBlockEntity::new,
                        GOLD_AUTO_FURNACE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_FURNACE_IDENTIFIER, GOLD_AUTO_FURNACE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_FURNACE_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_FURNACE_IDENTIFIER, new BlockItem(GOLD_AUTO_FURNACE_BLOCK, new FabricItemSettings()));

        // --- auto enchanting table ---

        //iron
        IRON_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoEnchantingTableBlockEntity::new,
                        IRON_AUTO_ENCHANTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER, IRON_AUTO_ENCHANTING_TABLE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_ENCHANTING_TABLE_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_ENCHANTING_TABLE_IDENTIFIER, new BlockItem(IRON_AUTO_ENCHANTING_TABLE_BLOCK, new FabricItemSettings()));

        //gold
        GOLD_AUTO_ENCHANTING_TABLE_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoEnchantingTableBlockEntity::new,
                        GOLD_AUTO_ENCHANTING_TABLE_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER, GOLD_AUTO_ENCHANTING_TABLE_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_ENCHANTING_TABLE_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_ENCHANTING_TABLE_IDENTIFIER, new BlockItem(GOLD_AUTO_ENCHANTING_TABLE_BLOCK, new FabricItemSettings()));

        //xp inside
        Registry.register(Registries.BLOCK, AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER, AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK);
        Registry.register(Registries.ITEM, AUTO_ENCHANTING_TABLE_XP_INSIDE_IDENTIFIER, new BlockItem(AUTO_ENCHANTING_TABLE_XP_INSIDE_BLOCK, new FabricItemSettings()));

        // --- auto experience orb vacuum ---

        //iron

        IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoExperienceOrbVacuumBlockEntity::new,
                        IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, new BlockItem(IRON_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK, new FabricItemSettings()));

        //gold

        GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoExperienceOrbVacuumBlockEntity::new,
                        GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_EXPERIENCE_ORB_VACUUM_IDENTIFIER, new BlockItem(GOLD_AUTO_EXPERIENCE_ORB_VACUUM_BLOCK, new FabricItemSettings()));

        // --- auto experience orb emitter ---

        //iron

        IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoExperienceOrbEmitterBlockEntity::new,
                        IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, new BlockItem(IRON_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK, new FabricItemSettings()));

        //gold

        GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoExperienceOrbEmitterBlockEntity::new,
                        GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_EXPERIENCE_ORB_EMITTER_IDENTIFIER, new BlockItem(GOLD_AUTO_EXPERIENCE_ORB_EMITTER_BLOCK, new FabricItemSettings()));

        // --- auto anvil ---

        //iron
        IRON_AUTO_ANVIL_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_ANVIL_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoAnvilBlockEntity::new,
                        IRON_AUTO_ANVIL_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_ANVIL_IDENTIFIER, IRON_AUTO_ANVIL_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_ANVIL_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_ANVIL_IDENTIFIER, new BlockItem(IRON_AUTO_ANVIL_BLOCK, new FabricItemSettings()));

        //gold
        GOLD_AUTO_ANVIL_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_ANVIL_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoAnvilBlockEntity::new,
                        GOLD_AUTO_ANVIL_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_ANVIL_IDENTIFIER, GOLD_AUTO_ANVIL_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_ANVIL_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_ANVIL_IDENTIFIER, new BlockItem(GOLD_AUTO_ANVIL_BLOCK, new FabricItemSettings()));

        ServerPlayNetworking.registerGlobalReceiver(UPDATE_AUTO_ANVIL_PACKET_ID, (server, player, handler, buf, responseSender) -> {
            int anvilMode = buf.readVarInt();

            server.executeSync(() -> {
                if (!player.isSpectator() && player.currentScreenHandler instanceof AutoAnvilScreenHandler screenHandler) {
                    screenHandler.setSelectedMode(anvilMode);
                }
            });
        });

        // --- auto brewing stand ---

        //iron
        IRON_AUTO_BREWING_STAND_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_AUTO_BREWING_STAND_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronAutoBrewingStandBlockEntity::new,
                        IRON_AUTO_BREWING_STAND_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_AUTO_BREWING_STAND_IDENTIFIER, IRON_AUTO_BREWING_STAND_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(IRON_AUTO_BREWING_STAND_BLOCK));
        Registry.register(Registries.ITEM, IRON_AUTO_BREWING_STAND_IDENTIFIER, new BlockItem(IRON_AUTO_BREWING_STAND_BLOCK, new FabricItemSettings()));

        //gold
        GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_AUTO_BREWING_STAND_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldAutoBrewingStandBlockEntity::new,
                        GOLD_AUTO_BREWING_STAND_BLOCK
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_AUTO_BREWING_STAND_IDENTIFIER, GOLD_AUTO_BREWING_STAND_BLOCK);
        ItemGroupEvents.modifyEntriesEvent(ITEM_GROUP).register(entries -> entries.add(GOLD_AUTO_BREWING_STAND_BLOCK));
        Registry.register(Registries.ITEM, GOLD_AUTO_BREWING_STAND_IDENTIFIER, new BlockItem(GOLD_AUTO_BREWING_STAND_BLOCK, new FabricItemSettings()));
    }
}
