package uk.co.cablepost.autoworkstations.auto_brewing_stand.gold;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.autoworkstations.AutoWorkstations;
import uk.co.cablepost.autoworkstations.auto_brewing_stand.AutoBrewingStandBlockEntity;

public class GoldAutoBrewingStandBlockEntity extends AutoBrewingStandBlockEntity {
    public GoldAutoBrewingStandBlockEntity(BlockPos pos, BlockState state){
        super(AutoWorkstations.GOLD_AUTO_BREWING_STAND_BLOCK_ENTITY, pos, state, 200);
    }
}
