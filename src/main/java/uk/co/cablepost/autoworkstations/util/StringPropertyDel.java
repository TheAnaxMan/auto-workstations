package uk.co.cablepost.autoworkstations.util;

public class StringPropertyDel {
    private String value;

    public StringPropertyDel(String initValue){
        value = initValue;
    }

    public String get(){
        if(value == null){
            return "unset";
        }

        return value;
    }

    public void set(String newValue){
        value = newValue;
    }
}
